package es.maincode.everis_clean_architecture.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import es.maincode.everis_clean_architecture.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
