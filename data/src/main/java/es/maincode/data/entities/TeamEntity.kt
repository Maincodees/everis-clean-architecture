package es.maincode.data.entities

data class TeamEntity (
    var city: String? = null,
    var fullName: String? = null,
    var teamId: String? = null,
    var nickName: String? = null,
    var logo: String? = null,
    var shortName: String? = null,
    var allStar: String? = null,
    var nbaFranchise: String? = null,
    var leagues: LeaguesEntity? = null
)