package es.maincode.data.entities

data class StandardEntity(
    var confName: String? = null,
    var divName: String? = null
)